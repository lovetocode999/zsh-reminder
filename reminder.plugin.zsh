# Data file for the todo list
# Format: (? means boolean)
# TASK,DAILY?,ACTIVE?,NUMBER,URGENT?
TODO_SAVE_FILE="$HOME/.todo.csv"
TODO_DATE_SAVE_FILE="$HOME/.todo.date"


function __todo_reload_daily() {
    # Check if the date has changed since the last time this function was run,
    # and, if it was, reload all the daily things
    TODO_DATE=$(date +%d)
    if [[ -f $TODO_DATE_SAVE_FILE ]]; then
        TODO_OLD_DATE=$(cat $TODO_DATE_SAVE_FILE)
    else
        TODO_OLD_DATE=$(($TODO_DATE - 1))
    fi
    if [ $TODO_DATE != $TODO_OLD_DATE ]; then
        awk '
        BEGIN {
            FPAT = "([^,]+)|(\"[^\"]+\")"
            count = 0
        }
        {
            if ($1 != "TASK") {
                if ($3 == "false" && $2 == "true") {
                    ++count
                    print $1","$2",true,"count","$5
                }
                else {
                    ++count
                    print $1","$2","$3","count","$5
                }
            }
            else {
                printf("TASK,DAILY?,ACTIVE?,NUMBER,URGENT?\n")
            }
        }
        ' $TODO_SAVE_FILE > "$TODO_SAVE_FILE.tmp"
        mv "$TODO_SAVE_FILE.tmp" $TODO_SAVE_FILE
        echo $TODO_DATE > $TODO_DATE_SAVE_FILE
    fi
}

function __increment_todo_number() {
    wc -l $TODO_SAVE_FILE | awk '{print $1}' | read TODO_NUMBER
}

function __load_tasks() {
    # Reload the daily stuff if it is the next day
    __todo_reload_daily
    # Load and print all tasks
    if [[ ! -f $TODO_SAVE_FILE ]]; then
        touch $TODO_SAVE_FILE
        echo "TASK,DAILY?,ACTIVE?,NUMBER,URGENT?" > $TODO_SAVE_FILE
    fi
    __increment_todo_number
    if [[ $TODO_NUMBER -gt 1 ]]; then
        TODO_TASK_LIST=$(awk '
        BEGIN {
            FPAT = "([^,]+)|(\"[^\"]+\")"
            count = 0
        }
        {
            if ($1 != "TASK" && $3 == "true") {
                if ($2 == "true") {
                    ++count
                    printf("\033[036m %s. %s\033[0m\n", $4, $1)
                }
                else if ($5 == "true") {
                    ++count
                    printf("\033[033m %s. %s\033[0m\n", $4, $1)
                }
                else {
                    ++count
                    printf(" %s. %s\n", $4, $1)
                }
            }
            else if ($1 == "TASK") {
                printf("\nTodo:\n")
            }
        }
        ' $TODO_SAVE_FILE)
        echo "$TODO_TASK_LIST"
    fi
}

function __save_tasks() {
    # Check if the provided arguments are valid
    if [ $# -eq 0 ]; then
        echo "Error: argument(s) expected, but none given"
        return 1
    fi
    # Check if first argument is "daily". If it is, add the rest of the
    # arguments to the daily todolist as a single task. Otherwise, add it to
    # the normal todolist. Also, check if the first argument is "urgent" and
    # mark it as urgent if it is
    __increment_todo_number
    if [[ -f "$TODO_SAVE_FILE" ]]; then
        if [[ "$1" == "daily" ]]; then
            echo "${@:2},true,true,${TODO_NUMBER},false" >> $TODO_SAVE_FILE
        elif [[ "$1" == "urgent" ]]; then
            echo "${@:2},false,true,${TODO_NUMBER},true" >> $TODO_SAVE_FILE
        else
            echo "$@,false,true,${TODO_NUMBER},false" >> $TODO_SAVE_FILE
        fi
    else
        touch $TODO_SAVE_FILE
        echo "TASK,DAILY?,ACTIVE?,NUMBER" >> $TODO_SAVE_FILE
        if [[ "$1" == "daily" ]]; then
            echo "${@:2},true,true,${TODO_NUMBER},false" >> $TODO_SAVE_FILE
        elif [[ "$1" == "urgent" ]]; then
            echo "${@:2},false,true,${TODO_NUMBER},true" >> $TODO_SAVE_FILE
        else
            echo "$@,false,true,${TODO_NUMBER},false" >> $TODO_SAVE_FILE
        fi
    fi
}

function __task_done() {
    # Check if the task is a daily task. If it is, flag it as done in .todo.csv
    # Otherwise, remove it from .todo.csv completely
    awk '
    BEGIN {
        FPAT = "([^,]+)|(\"[^\"]+\")"
        pattern = "'"${@}"'"
        count = 0
    }
    {
        if ($1 != "TASK") {
            if ($4 == pattern) {
                if ($2 == "true") {
                    ++count
                    print $1","$2",false,"count",false"
                }
            } 
            else {
                ++count
                print $1","$2","$3","count","$5
            }
        }
        else {
            printf("TASK,DAILY?,ACTIVE?,NUMBER,URGENT?\n")
        }
    }
    END {}
    ' $TODO_SAVE_FILE > "$TODO_SAVE_FILE.tmp"
    mv "$TODO_SAVE_FILE.tmp" $TODO_SAVE_FILE
}


# Add aliases and functions to access these hidden functions. 
alias todo="__save_tasks"

function task_done() {
    if ! [ $# -eq 0 ]; then
        if [ $# -gt 1 ]; then
            echo "Error: too many arguments"
            return 1
        fi
        if ! [[ $1 =~ '^[0-9]+$' ]]; then
            echo "Error: expected integer, got string"
            return 1
        fi
        __increment_todo_number
        if [[ $1 -gt $TODO_NUMBER ]]; then
            echo "Error: there are only ${TODO_NUMBER} todo list entries"
            return 1
        fi
        __task_done "$@"
    else
        echo "Error: argument expected, but none given"
        return 1
    fi
}


# Register a ZSH hook to print the todo list after every command, before every
# prompt
autoload -U add-zsh-hook
add-zsh-hook precmd __load_tasks


# Also, this is probably the point to register completions. Will have to 
# research...

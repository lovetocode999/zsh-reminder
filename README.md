# zsh-reminder
A simple ZSH plugin which displays reminders above every prompt

This small plugin allows your terminal to remind you that you have something to do. It is a complete rewrite of [oh-my-zsh-reminder](https://github.com/AlexisBRENON/oh-my-zsh-reminder) by [Alexis BRENON](https://github.com/AlexisBRENON)

## Usage

[![asciicast](https://asciinema.org/a/360807.svg)](https://asciinema.org/a/360807)

## Installation

##### Antibody:
Unfortunately, antibody installs plugins from GitHub repositories only, and this is a GitLab repository. 

##### Antigen:
Unfortunately, antigen installs plugins from GitHub repositories only, and this is a GitLab repository. 

##### dotzsh
Clone this repo into .zsh.local/modules, then load the plugin module in your .zshrc

##### Manual:
Clone this repo into your plugins folder, then source reminder.plugin.zsh in your .zshrc

##### Oh-My-ZSH:
Clone this repo into your plugins folder, then add it to the list of plugins in your .zshrc

##### Prezto:
Clone this repo into your prezto modules directory, then add the plugin to your .zpreztorc file

##### Zinit:
```zinit from"gl" "lovetocode999/zsh-reminder"```

##### ZPM
```zpm "lovetocode999/zsh-reminder",type:gitlab```
